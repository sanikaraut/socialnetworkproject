<?php
	require_once('class-db.php');

	if ( !class_exists('QUERY') ) {
		class QUERY {

			public function load_user_object($userId) {
				global $db;

				$table = 'users';

				$query = "
								SELECT * FROM $table

							";

				$obj = $db->select($query);

				if ( !$obj ) {
					return "No user found";
				}

				return $obj[0];
			}

			public function load_all_user_objects() {
				global $db;

				$table = 'users';

				$query = "
								SELECT * FROM $table
							";

				$obj = $db->select($query);

				if ( !$obj ) {
					return "No user found";
				}

				return $obj;
			}


			public function get_friends($user_id) {
				global $db;

				$table = 's_friends';

				$query = "
								SELECT ID, friend_id FROM $table
								WHERE user_id = '$user_id'
							";

				$friends = $db->select($query);

				foreach ( $friends as $friend ) {
					$friend_ids[] = $friend->friend_id;
				}

				return $friend_ids;
			}

			public function get_status_objects($user_id) {
				global $db;

				$table = 's_status';

				$friend_ids = $this->get_friends($user_id);

				if ( !empty ( $friend_ids ) ) {
					array_push($friend_ids, $user_id);
				} else {
					$friend_ids = array($user_id);
				}

				$accepted_ids = implode(', ', $friend_ids);

				$query = "
								SELECT * FROM s_status A,users B
								WHERE A.user_id IN ($accepted_ids) AND A.user_id = B.userId
								ORDER BY status_time DESC
							";

				$status_objects = $db->select($query);

				return $status_objects;
			}


			public function do_user_directory() {
				$users = $this->load_all_user_objects();

				foreach ( $users as $user ) { ?>
					<div class="directory_item">
						<h3><a href="/social/profile-view.php?uid=<?php echo $user->ID; ?>"><?php echo $user->userName; ?></a></h3>
						<p><?php echo $user->userEmail; ?></p>
					</div>
				<?php
				}
			}

			public function do_friends_list($friends_array) {
				foreach ( $friends_array as $friend_id ) {
					$users[] = $this->load_user_object($friend_id);
				}

				foreach ( $users as $user ) { ?>
					<div class="directory_item">
						<h3><a href="/social/profile-view.php?uid=<?php echo $user->ID; ?>"><?php echo $user->userName; ?></a></h3>
						<p><?php echo $user->userEmail; ?></p>
					</div>
				<?php
				}
			}

			public function do_news_feed($user_id) {
				$status_objects = $this->get_status_objects($user_id);

				foreach ( $status_objects as $status ) {?>
					<div class="status_item">
						<!-- <?php print_r ($status);?> -->
						<?php $user = $this->load_user_object($status->user_id); ?>
						<h3><a href="/social/profile-view.php?uid=<?php echo $user->ID; ?>"><!-<?php echo isset($status->userName)?$status->userName :''; ?>-!></a></h3>
						<p><?php echo $status->status_content; ?></p>
					</div>
				<?php
				}
			}
		}
	}

	$query = new QUERY;
?>
