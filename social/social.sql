-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 12, 2016 at 05:04 PM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 7.0.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `social`
--

-- --------------------------------------------------------

--
-- Table structure for table `s_friends`
--

CREATE TABLE `s_friends` (
  `ID` int(22) NOT NULL,
  `user_id` int(22) NOT NULL,
  `friend_id` int(22) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `s_friends`
--

INSERT INTO `s_friends` (`ID`, `user_id`, `friend_id`) VALUES
(0, 1, 3),
(0, 1, 2),
(0, 1, 0),
(0, 1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `s_status`
--

CREATE TABLE `s_status` (
  `ID` int(22) NOT NULL,
  `user_id` int(22) NOT NULL,
  `status_time` time(6) NOT NULL,
  `status_content` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `s_status`
--

INSERT INTO `s_status` (`ID`, `user_id`, `status_time`, `status_content`) VALUES
(1, 1, '838:59:59.999999', 'HEY'),
(2, 1, '838:59:59.999999', 'I am loving it!'),
(3, 1, '838:59:59.999999', 'Hey'),
(4, 1, '838:59:59.999999', 'hi\r\n'),
(5, 1, '00:00:00.000000', 'hello'),
(6, 1, '838:59:59.999999', 'helloooo'),
(7, 1, '00:00:00.000000', 'zzzzzz\r\n'),
(8, 1, '00:00:00.000000', 'eewww'),
(9, 1, '00:00:00.000000', 'yo yo\r\n'),
(10, 1, '00:00:00.000000', 'hmmm nnn\r\n'),
(11, 1, '838:59:59.999999', 'mmmm'),
(12, 1, '00:00:00.000000', 'Good morning'),
(13, 1, '838:59:59.999999', 'bbbb'),
(14, 1, '838:59:59.999999', 'hey hi\r\n'),
(15, 1, '838:59:59.999999', 'nnn\r\n'),
(16, 1, '838:59:59.999999', 'kmkmk'),
(17, 1, '838:59:59.999999', 'hey malli here');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `userId` int(11) NOT NULL,
  `userName` varchar(30) NOT NULL,
  `userEmail` varchar(60) NOT NULL,
  `userPass` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userId`, `userName`, `userEmail`, `userPass`) VALUES
(1, 'xyz', 'xyz@gmail.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92'),
(2, 'SANIKA', 'sani@gmail.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92'),
(3, 'isha', 'isha@gmail.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92'),
(4, 'Varsha Raut', 'varsha@gmail.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92'),
(5, 'Pravin Raut', 'pravin@gmail.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92'),
(6, 'Swarna', 'swarna@gmail.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92'),
(7, 'SANIKA RAUT', 'sanika.raut0@gmail.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92'),
(8, 'John Clay', 'john@gmail.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92'),
(9, 'Malli', 'malli@gmail.com', '72cdef995437961f74259535383bd2a0351da3ce886a2ce117a48d81e6d163b4');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `s_status`
--
ALTER TABLE `s_status`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userId`),
  ADD UNIQUE KEY `userEmail` (`userEmail`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `s_status`
--
ALTER TABLE `s_status`
  MODIFY `ID` int(22) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
