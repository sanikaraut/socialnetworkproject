<?php

	define('DBHOST', 'localhost');
	define('DBUSER', 'root');
	define('DBPASS', '');
	define('DBNAME', 'social');

	$conn = mysqli_connect('localhost','root','');
	$dbcon = mysqli_select_db($conn,'social');

	if ( !$conn ) {
		die("Connection failed : " . mysqli_error());
	}

	if ( !$dbcon ) {
		die("Database Connection failed : " . mysqli_error());
	}
